import React from "react";

export class PureComponent extends React.PureComponent {
  render() {
    return <h3>I'm a Pure Component</h3>;
  }
}
