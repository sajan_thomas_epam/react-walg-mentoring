import PropTypes from "prop-types";

import style from "./dropdown.module.scss";

export const Dropdown = ({ dropdownData, handleChange, customStyle }) => {
  if (dropdownData.length) {
    return (
      <select className={customStyle ?? style.dropdown}>
        {dropdownData.map((item) => (
          <option key={item.value} value={item.value}>
            {item.label}
          </option>
        ))}
      </select>
    );
  }

  return <p>No Data</p>;
};

Dropdown.propTypes = {
  dropdownData: PropTypes.array.isRequired,
  handleClick: PropTypes.func,
  customStyle: PropTypes.string,
};

Dropdown.defaultProps = {
  dropdownData: [],
  handleChange: () => {},
  customStyle: null,
};
