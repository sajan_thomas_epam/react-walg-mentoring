import { Card } from "../../components/Card";
import { Dropdown } from "../../components/Dropdown/Dropdown.component";
import { TabFilter } from "../../components/TabFilter";
import Layout from "../../Layout";
import style from "./homepage.module.scss";

const dropDownData = [
  {
    label: "Release Date",
    value: "release_date",
  },
  {
    label: "Budget",
    value: "budget",
  },
];

export const HomePageContainer = () => {
  return (
    <Layout>
      <div className={style.homePageTopWrapper}>
        <TabFilter />
        <div>
          <span> SORT BY </span>{" "}
          <Dropdown dropdownData={dropDownData} customStyle={style.dropdown} />
        </div>
      </div>
      39 movies found
      <div className={style.cardHolder}>
        <Card name="SAM" year="2040" genre="Action" />
      </div>
    </Layout>
  );
};
