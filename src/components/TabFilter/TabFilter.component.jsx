import styles from "./tabfilter.module.scss";

const tabData = [
  {
    label: "All",
    accessor: "all",
  },
  {
    label: "Documentary",
    accessor: "documentary",
  },
  {
    label: "Comedy",
    accessor: "comedy",
  },
  {
    label: "Horror",
    accessor: "horror",
  },
  {
    label: "Crime",
    accessor: "crime",
  },
];

export const TabFilter = () => (
  <ul className={styles.tab}>
    {tabData.map((tabItem) => (
      <li key={tabItem.accessor}>{tabItem.label}</li>
    ))}
  </ul>
);
