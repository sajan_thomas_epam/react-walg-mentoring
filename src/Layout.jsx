import { ErrorBoundary } from "./components/ErrorBoundary/Error.component";
import { Footer } from "./components/Footer";
import { Header } from "./components/Header";

function Layout({ children }) {
  return (
    <>
      <ErrorBoundary>
        <Header />
        <div className="base-margin"> {children}</div>
        <Footer />
      </ErrorBoundary>
    </>
  );
}

export default Layout;
