import React from "react";
import PropTypes from "prop-types";

import style from "./card.module.scss";
import bg from "../../assets/rap.png";
import { Button } from "../Button";

export const Card = ({ name, year, genre }) => {
  //   const [error, setError] = React.useState(false);
  //   const throwError = () => {
  //     setError(true);
  //   };

  //   if (error) throw new Error("Error check");

  return (
    <div className={style.cardWrapper}>
      <img src={bg} alt="Movie Poster" />
      <div className={style.cardMovieNameConatiner}>
        <h3>{name}</h3>
        <span>{year}</span>
      </div>
      <h5>{genre}</h5>
      <Button
        btnText="..."
        customStyle={style.button}
        // handleClick={throwError}
      />
    </div>
  );
};

Card.propTypes = {
  name: PropTypes.string.isRequired,
  year: PropTypes.string.isRequired,
  genre: PropTypes.string.isRequired,
};

Card.defaultProps = {
  name: "NA",
  year: "NA",
  genre: "NA",
};
