import React from "react";

import { Button } from "../Button";
import styles from "./header.module.scss";

export const Header = () => {
  return (
    <div className={styles.headerContainer}>
      <div className={styles.headerTopBar}>
        <h1>netflixroulette</h1>
        <Button
          btnText="+ Add Movie"
          customStyle={styles.baseButton_addMovie}
        />
      </div>
      <div className={styles.findMovieContainer}>
        <h1 className={styles.title}>Find Your Movie</h1>
        <input
          placeholder="what do you want to watch"
          className={styles.searchField}
        />
        <Button btnText="Search" customStyle={styles.baseButton_searchMovie} />
      </div>
    </div>
  );
};
