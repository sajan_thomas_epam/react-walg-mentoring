import React from "react";

export class ClassComponent extends React.Component {
  render() {
    return <h3>I'm Class Component</h3>;
  }
}
