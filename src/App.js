import React from "react";
import "./global.scss";

import { HomePage } from "./containers/Home";

function App() {
  return (
    <React.StrictMode>
      <HomePage />
    </React.StrictMode>
  );
}

export default App;
