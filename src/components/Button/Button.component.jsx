import PropTypes from "prop-types";

import style from "./button.module.scss";

export const Button = ({ btnText, handleClick, customStyle }) => {
  return (
    <button className={customStyle ?? style.button} onClick={handleClick}>
      {btnText}
    </button>
  );
};

Button.propTypes = {
  btnText: PropTypes.string.isRequired,
  handleClick: PropTypes.func,
  customStyle: PropTypes.string,
};

Button.defaultProps = {
  btnText: "Button",
  handleClick: () => {},
  customStyle: null,
};
